import asyncio
import logging
logging.basicConfig(level=logging.DEBUG)



async def tcp_echo_client(message, loop):
    reader, writer = await asyncio.open_connection('127.0.0.1', 8888, loop=loop)
    logging.info('Send: %r' % message)
    writer.write(message.encode())

    data = await reader.read(100)
    logging.info('Received: %r' % data.decode())
    logging.info('Close the socket')
    writer.close()

message = 'Hello World!'
loop = asyncio.get_event_loop()
loop.run_until_complete(tcp_echo_client(message, loop))
loop.close()
