# © 2021 Tobias Zehntner
# License AGPL-3.0 or later (https://www.gnu.org/licenses/agpl.html).

import argparse
import asyncio
import logging
import sys

from halo_controller import SETTINGS
from halo_unit import HaloUnit


class ArgParser(argparse.ArgumentParser):
    def error(self, message):
        sys.stderr.write("error: %s\n" % message)
        self.print_help(sys.stderr)
        sys.exit(2)


def setup_main_logging():
    FORMAT = logging.Formatter("%(asctime)s - %(levelname)s - %(message)s")
    console_handler = logging.StreamHandler()
    console_handler.setFormatter(FORMAT)
    file_handler = logging.FileHandler(f"logs/unit-{unit.id}.log", mode="a")
    file_handler.setFormatter(FORMAT)
    logger.setLevel(SETTINGS["logging"]["level"])
    logger.addHandler(console_handler)
    logger.addHandler(file_handler)


logger = logging.getLogger("SYSTEM")

parser = ArgParser(description="Run Halo Unit")
parser.add_argument("unit_id", help="Unit ID")
parser.add_argument("-c", "--controller-hostname")
args = parser.parse_args()

unit_id = args.unit_id
unit = HaloUnit(unit_id)
if args.controller_hostname:
    unit.controller_hostname = args.controller_hostname


async def setup():
    logger.info(f"Starting {unit.name}...")
    setup_main_logging()
    await unit.setup()
    logger.info("Setup finished")


async def run():
    await setup()
    await asyncio.gather(unit.run())


def empty_queue(q):
    for _ in range(q.qsize()):
        # Depending on your program, you may want to
        # catch QueueEmpty
        q.get_nowait()
        q.task_done()


async def shut_down(unit):
    logger.info("Shutting down...")
    try:
        loop = asyncio.get_running_loop()
    except RuntimeError:
        loop = asyncio.new_event_loop()

    empty_queue(unit.queue)

    serial_task = loop.create_task(unit.arduino.run_serial())
    shut_down_task = loop.create_task(unit.shut_down())
    await shut_down_task
    serial_task.cancel()
    await serial_task
    return


async def main():
    # FIXME avoid SIGTERM to interrupt shutdown
    unit.queue = asyncio.Queue()
    try:
        await asyncio.gather(unit.arduino.run_serial(), run())
    except asyncio.CancelledError:
        pass
    except KeyboardInterrupt:
        pass
    except BaseException as e:
        logger.exception(e)
    finally:
        await shut_down(unit)
        logger.info(f"{unit.name} turned off.")


if __name__ == "__main__":
    try:
        asyncio.run(main())
    except KeyboardInterrupt:
        pass
