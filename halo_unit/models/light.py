# © 2021 Tobias Zehntner
# License AGPL-3.0 or later (https://www.gnu.org/licenses/agpl.html).

import asyncio

from halo_controller import SETTINGS, Base, helper


class Light(Base):
    def __init__(self, unit):
        super().__init__()
        self.unit = unit

    async def off(self):
        await self.set_dim_value(SETTINGS["hardware"]["light_off"])

    async def on(self):
        await self.set_dim_value(SETTINGS["hardware"]["light_on"])

    async def set_brightness(self, percent):
        """
        :param percent: 0-100%
        :type percent: int
        """
        # TODO use adjustable curve
        # Use the upper range of an exponential curve, for a smooth dim
        new_percent = helper.map_range(percent, 0, 100, 50, 100)
        exponent = (100 ** (1 / 100)) ** new_percent

        dim_value = int(
            helper.map_range(
                exponent,
                0,
                100,
                SETTINGS["calibration"]["min_dim"],
                SETTINGS["calibration"]["max_dim"],
                clamp=True,
            )
        )
        await self.set_dim_value(dim_value)
        return

    async def set_dim_value(self, dim_value):
        await self.unit.arduino.command("dim_value", dim_value)

    async def run_demo(self):
        """
        Dim up and down between 0 and 100% for test purposes
        """
        brightness = 50
        diff = 2
        while True:
            if not 0 < brightness < 100:
                diff *= -1
            brightness += diff
            await self.set_brightness(brightness)
            await asyncio.sleep(SETTINGS["calibration"]["update_light_sec"])
