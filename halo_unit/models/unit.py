# © 2021 Tobias Zehntner
# License AGPL-3.0 or later (https://www.gnu.org/licenses/agpl.html).

import asyncio
from datetime import datetime

from halo_controller import SETTINGS, Base, helper

from .client import Client
from .light import Light
from .motor import Motor
from .serial_connector import SerialConnector


class HaloUnit(Base):
    def __init__(self, unit_id):
        super().__init__()
        self.id = int(unit_id)

        # TODO load settings to class at init, make it updateable from controller
        unit_settings = SETTINGS["units"][self.id]
        self.hostname = unit_settings["hostname"]
        self.name = unit_settings["name"]
        self.grid_pos_x = unit_settings["grid_pos_x"]
        self.grid_pos_y = unit_settings["grid_pos_y"]

        self.arduino = SerialConnector(self)
        self.motor = Motor(self)
        self.light = Light(self)
        self.client = Client(self)

        self.min_distance = False
        self.queue = False

        self.position_data = {}

    async def setup(self):
        tasks = [self.client.setup(), self.motor.start()]
        await asyncio.wait(tasks, return_when=asyncio.ALL_COMPLETED)

    async def run(self):
        self.logger.info(f"Running {self.name}")
        tasks = [
            self.update_light(),
            self.get_current_unit_position_data(),
            self.update_units_position_data(),
        ]
        await asyncio.gather(*tasks)

    async def shut_down(self):
        # TODO is not shut down when RPi is rebooted
        self.logger.debug("Turning off unit...")
        await self.arduino.command("shut_down")
        return

    async def update_light(self):
        while True:
            brightness = 0
            if len(self.position_data.keys()) > 1:
                # At least one other unit is running
                distances = self.get_distances()
                if distances:
                    current_min_distance = min(distances)
                    if (
                        not self.min_distance
                        or self.min_distance > current_min_distance
                    ):
                        self.min_distance = current_min_distance

                    brightness = helper.map_range(
                        min(distances),
                        SETTINGS["calibration"]["max_distance"],
                        self.min_distance,
                        0,
                        100,
                        clamp=True,
                    )

            await self.light.set_brightness(brightness)
            await asyncio.sleep(SETTINGS["calibration"]["update_light_sec"])

    def get_distances(self):
        current_positions = helper.compute_current_positions(
            self.position_data, compute_ahead=True
        )
        unit_pos = current_positions[self.id]
        distances = []
        for unit_id in current_positions.keys():
            if unit_id == self.id:
                continue
            distances.append(
                helper.get_distance_between_points(unit_pos, current_positions[unit_id])
            )
        return distances

    async def get_current_unit_position_data(self):
        while True:
            request_time = datetime.now()
            current_pos, steps_per_second_real = await self.arduino.request(
                "motor_current_pos"
            )
            self.motor.set_rpm_current_real(
                current_pos, steps_per_second_real, request_time
            )

            # if not self.motor.is_accelerating:
            #     await self.motor.adapt_motor_speed()

            await asyncio.sleep(SETTINGS["communication"]["own_unit_update_rate_sec"])

    async def update_units_position_data(self):
        while True:
            if self.motor.last_pos:

                # Compute own unit data
                current_pos = self.motor.last_pos
                request_time = self.motor.last_request_time

                motor_angle = self.motor.get_motor_angle(current_pos)
                pos_relative = helper.get_relative_position(
                    motor_angle, SETTINGS["units"][self.id]["target_radius"]
                )
                rpm = (
                    self.motor.rpm_current_real
                    if self.motor.rpm_current_real
                    else self.motor.rpm_current
                )
                # Send data to controller
                command = "post_data"
                unit_data = {
                    "timestamp": request_time.isoformat(),
                    "motor_angle": motor_angle,
                    "rpm": rpm,
                    "radians_per_second": helper.rpm_to_radians_per_second(
                        rpm, self.motor.is_dir_ccw
                    ),
                    "pos_relative": pos_relative,
                }

                response = await self.client.send_and_receive(command, unit_data)

                # Collect other unit data
                if response:
                    if response["status"] == "ok":
                        if response.get("data"):
                            # Convert dict keys (unit ids) to ints
                            data = {int(k): v for k, v in response["data"].items()}
                            self.position_data = data
                    else:
                        self.logger.error(
                            f"Error transferring data to controller: {response['msg']}"
                        )
                else:
                    self.logger.error("No response from controller")

            await asyncio.sleep(
                SETTINGS["communication"]["other_units_update_rate_sec"]
            )
