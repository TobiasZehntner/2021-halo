# © 2021 Tobias Zehntner
# License AGPL-3.0 or later (https://www.gnu.org/licenses/agpl.html).


import asyncio
import os

import aioserial

from halo_controller import SETTINGS, Base

COMMAND = {
    "dim_value": 1,
    "motor_micros_between_steps": 2,
    "motor_current_pos": 3,
    "shut_down": 9,
}


class SerialConnector(Base):
    def __init__(self, unit):
        super().__init__()
        self.unit = unit
        self.serial_connection = False
        self.responses = {}
        try:
            if os.path.exists("/dev/ttyACM0"):
                self.open_serial("/dev/ttyACM0")
            elif os.path.exists("/dev/ttyAMA0"):
                self.open_serial("/dev/ttyAMA0")
            else:
                raise Exception("Could not connect to Arduino")
        except aioserial.serialutil.SerialException as e:
            raise Exception(f"Could not connect to Arduino ({e})")

    def open_serial(self, path):
        self.serial_connection = aioserial.AioSerial(
            path, SETTINGS["hardware"]["serial_baud_rate"], timeout=1, write_timeout=1
        )

        self.serial_connection.flush()
        self.serial_connection.reset_input_buffer()

    async def request(self, command):
        """
        Ask for data from Arduino
        """
        value = await self.command(command, is_request=True)
        return value

    async def command(self, command, value=0, is_request=False):
        """
        Queue a command to Arduino and for requests, return value when available
        """
        cmd = COMMAND[command]
        event = asyncio.Event()
        await self.unit.queue.put((event, cmd, value, is_request))
        await event.wait()
        if is_request:
            return self.responses[cmd]

    async def send_receive(self, bytes_out):
        """
        Send data until there is a response
        """
        bytes_in = False
        try:
            while not bytes_in:
                await self.serial_connection.write_async(bytes_out)
                bytes_in = await self.serial_connection.readline_async()
                if not bytes_in:
                    # Read a second time before resending
                    bytes_in = await self.serial_connection.readline_async()
        except IOError as e:
            self.logger.error(f"Failed to read data from Arduino ({e})")
            return
        return bytes_in

    async def run_serial(self):
        """
        Process command as soon as there is one in the queue:
        1. Send command to Arduino
        2. Read response
        3. Assert that response is correct
        4. For request, save value in dict
        5. Signal to requestor that command has been processed and data is available,
            so they can continue
        """
        while True:
            event, cmd, value, is_request = await self.unit.queue.get()
            string = f"{cmd}/{int(value)}\n"
            bytes_out = string.encode("utf-8")
            bytes_in = await self.send_receive(bytes_out)
            if bytes_in:
                data = bytes_in.decode("utf-8").strip().split("/")
                try:
                    cmd_return, values = (
                        int(data[0]),
                        [int(d) for d in data[1].split(",") if d] if data[1] else [0],
                    )
                    assert (
                        cmd == cmd_return
                    ), f"Wrong responsee from Arduino: Sent: {cmd}, Returned: {cmd_return}"
                    if is_request:
                        response = values[0] if len(values) == 1 else values
                        self.responses[cmd] = response
                except ValueError as e:
                    # Arduino gave back wrong byte. Ignore, so previous value will be taken
                    self.logger.error(f"Communication error with Arduino ({e})")
                    pass
            event.set()
            self.unit.queue.task_done()
