# © 2021 Tobias Zehntner
# License AGPL-3.0 or later (https://www.gnu.org/licenses/agpl.html).

import asyncio
import json
import socket

from halo_controller import SETTINGS, Base


class Client(Base):
    def __init__(self, unit):
        super().__init__()
        self.controller_hostname = None
        self.is_connected = False
        self.unit = unit

    async def connect(self):
        """
        The controller can run on the Mac or RPi and might have different hostnames.
        Try each until successful connection is made, then save hostname.
        """
        while not self.is_connected:
            possible_hostnames = SETTINGS["controller"]["hostnames"]
            self.logger.info("Searching controller...")
            for hostname in possible_hostnames:
                self.controller_hostname = hostname
                try:
                    response = await self.connect_call()
                    if response and response["status"] == "ok":
                        self.is_connected = True
                        self.logger.info(f"Connected to {self.controller_hostname}")
                        break
                    else:
                        self.logger.error(
                            f"No response from {self.controller_hostname}"
                        )
                        continue
                except ConnectionRefusedError as e:
                    self.logger.warning(
                        f"Host {self.controller_hostname} found but controller not "
                        f"running ({e.strerror})"
                    )
                    continue
                except asyncio.TimeoutError:
                    self.logger.debug(
                        f"Timeout on connect call to {self.controller_hostname}"
                    )
                    continue
                except (OSError, socket.gaierror) as e:
                    # Host not found, try another hostname
                    self.logger.debug(
                        f"No host found on {self.controller_hostname}, trying another "
                        f"({e.strerror})"
                    )
                    continue
            if not self.is_connected:
                self.controller_hostname = None
                self.logger.error(
                    "Controller not found on this network. Trying again in 5 seconds."
                )
                await asyncio.sleep(5)
        return

    def disconnect(self):
        self.is_connected = False
        self.controller_hostname = None

    async def connect_call(self):
        cmd = "connect"
        bytes_out = self.get_bytes_out(cmd)
        reader, writer = await asyncio.open_connection(self.controller_hostname, 8888)
        writer.write(bytes_out)
        bytes_in = await asyncio.wait_for(reader.readuntil(b"\n"), timeout=5)
        if not bytes_in:
            return
        response = json.loads(bytes_in.decode())
        return response

    def get_bytes_out(self, cmd, values=None):
        data_out = {"unit_id": self.unit.id, "command": cmd, "data": values}
        json_file = json.dumps(data_out) + "\n"
        bytes_out = json_file.encode()
        return bytes_out

    async def send_and_receive(self, cmd, values=None):
        if not self.is_connected:
            await self.connect()

        bytes_out = self.get_bytes_out(cmd, values)
        bytes_in = await self.server_write_read(bytes_out)

        if not bytes_in:
            self.logger.error("No response from server")
            return

        response = json.loads(bytes_in.decode())
        if response["status"] == "error":
            self.logger.error(response["msg"])
            return
        self.logger.debug("Exchanged data with controller")
        return response

    async def server_write_read(self, bytes_out):
        try:
            reader, writer = await asyncio.open_connection(
                self.controller_hostname, 8888
            )
            writer.write(bytes_out)
            try:
                return await asyncio.wait_for(reader.readuntil(b"\n"), timeout=5)
            except asyncio.TimeoutError:
                self.logger.warning("Reached timeout when reading from controller")
            finally:
                writer.close()
                await writer.wait_closed()
        except asyncio.TimeoutError:
            self.logger.warning("Reached timeout when connecting to controller")
        except (
            ConnectionRefusedError,
            ConnectionResetError,
            socket.gaierror,
            OSError,
        ) as e:
            self.logger.warning(
                f"Connection to controller lost: {self.controller_hostname} "
                f"({e.strerror})"
            )
            self.disconnect()

    async def setup(self):
        await self.connect()
        return
