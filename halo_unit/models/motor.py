# © 2021 Tobias Zehntner
# License AGPL-3.0 or later (https://www.gnu.org/licenses/agpl.html).

import asyncio
from datetime import datetime

from halo_controller import SETTINGS, Base, helper


class Motor(Base):
    def __init__(self, unit):
        super().__init__()
        self.unit = unit
        self.steps_per_round = SETTINGS["hardware"]["motor_driver_steps_per_round"]
        self.target_radius = SETTINGS["units"][self.unit.id]["target_radius"]
        cable_length = SETTINGS["units"][self.unit.id]["cable_length_m"]
        motor_arm_length = SETTINGS["hardware"]["motor_arm_length"]
        angle = helper.get_angle(cable_length, self.target_radius, motor_arm_length)
        self.rpm_target = helper.get_rpm(cable_length, angle, motor_arm_length)
        self.rpm_current = 0
        self.rpm_current_real = 0
        self.is_dir_ccw = False  # Warning: must be same on arduino
        self.last_pos = 0
        self.last_request_time = False
        self.is_accelerating = False
        self.last_speed_change = datetime.now()

    def get_motor_angle(self, motor_pos):
        """
        M
        :param motor_pos: current motor position read by arduino
        :type motor_pos: int
        :return: Motor angle in degrees
        :rtype: float
        """
        offset_degrees = SETTINGS["units"][self.unit.id]["motor_zero_offset_degrees"]
        motor_angle = ((360 / self.steps_per_round * motor_pos) + offset_degrees) % 360
        return motor_angle

    async def set_speed(self, rpm):
        micros_between_steps = 0
        if rpm > 0:
            micros_between_steps = helper.rpm_to_micros(rpm)
        self.rpm_current = rpm
        await self.unit.arduino.command(
            "motor_micros_between_steps", micros_between_steps
        )
        return

    async def go_to_speed(self, rpm_target):
        self.is_accelerating = True
        speed = self.rpm_current
        if not speed:
            # There can be a bump when starting the motor. Wait before increasing
            speed = 0.05
            await self.set_speed(speed)
            await asyncio.sleep(5)
        while self.rpm_current < rpm_target:
            await self.set_speed(speed)
            speed += 0.05
            await asyncio.sleep(0.4)
        self.is_accelerating = False
        self.last_speed_change = datetime.now()
        return

    async def start(self):
        """
        Accelerate motor until target speed is reached.
        """
        # Safety wait so there is a break if it's restarted directly from running
        await self.set_speed(0)
        self.logger.info("Waiting to start...")
        await asyncio.sleep(20)
        self.logger.debug("Accelerating motor...")
        await self.go_to_speed(self.rpm_target)
        self.logger.info(
            f"Reached target speed at {'%.2f' % self.rpm_target}rpm "
            f"for radius {self.target_radius}m"
        )

    async def adapt_motor_speed(self):
        now = datetime.now()
        if self.rpm_current_real and (now - self.last_speed_change).seconds > 10:
            # Only adapt speed every 10s to give bulb time to adapt
            if (
                self.rpm_target - self.rpm_current_real > 0.05
                and abs(self.rpm_target - self.rpm_current) < 2
            ):
                await self.set_speed(self.rpm_current + 0.05)
                self.logger.debug(
                    f"Increased rpm to {'%.2f' % self.rpm_current} "
                    f"(Target: {'%.2f' % self.rpm_target}, "
                    f"Real: {'%.2f' % self.rpm_current_real})"
                )
            elif self.rpm_current_real - self.rpm_target > 0.5:
                await self.set_speed(self.rpm_current - 0.5)
                self.logger.debug(
                    f"Decreased rpm to {'%.2f' % self.rpm_current} "
                    f"(Target: {'%.2f' % self.rpm_target}, "
                    f"Real: {'%.2f' % self.rpm_current_real})"
                )
            self.last_speed_change = now

    def set_rpm_current_real(
        self, current_pos, steps_per_second_real, current_request_time
    ):
        if self.last_pos:
            rpm = helper.steps_to_rpm(steps_per_second_real)
            if abs(rpm - self.rpm_current) < 4:
                # Only set the rpm if it's not completely off to avoid errors from
                # wrong motor zero interrupt triggers
                self.rpm_current_real = rpm
            else:
                self.logger.warning(
                    f"Measured RPM seems wrong. Ignored. "
                    f"(Expected: {'%.2f' % self.rpm_current}, Measured: {'%.2f' % rpm})"
                )
        self.last_pos = current_pos
        self.last_request_time = current_request_time
