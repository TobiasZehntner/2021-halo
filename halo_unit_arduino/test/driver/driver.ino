#include "Wire.h"
#include "AccelStepper.h"
#include "digitalWriteFast.h"
#include "EveryTimerB.h"
#define Timer1 TimerB2

#define DEBUG true

#define PIN_DIM_ZERO_CROSS 2  // Interrupt 0
#define PIN_DIM_PWM  4
#define PIN_MOTOR_ENABLE 5
#define PIN_MOTOR_DIR 6
#define PIN_MOTOR_PULSE 7
#define PIN_MOTOR_ZERO 8

#define DIM_FREQ_STEP 75  // EU 50Hz: 75; US 60Hz: 65

#define MOTOR_ACCELERATION 50
#define MOTOR_SPEED_MAX 1000000
long STEPS_PER_ROUND = 800;

long rpm = 20;

long motor_steps_per_minute = rpm * STEPS_PER_ROUND;
//long motor_steps_per_second = motor_steps_per_minute / 60;
long motor_steps_per_second = 500;

#define MOTOR_ACCELERATION motor_steps_per_second  // Accelerate to full speed in 1s

#define I2C_ARDUINO_ADDRESS 0x04

AccelStepper stepper(1, PIN_MOTOR_PULSE, PIN_MOTOR_DIR);

int dim_value = 128;
volatile int dim_counter = 0;
volatile boolean dim_zero_cross = false;

bool motor_on = false;
int motor_pos = 10;
bool motor_is_homed = false;

long time_passed = 0;
 int last_pos = 0;
long current_millis = 0;

void setup() {
  if (DEBUG) {
      Serial.begin(9600);
  }

  Wire.begin(I2C_ARDUINO_ADDRESS);
  Wire.onReceive(i2c_receive);
  Wire.onRequest(i2c_send);

  stepper.setEnablePin(PIN_MOTOR_ENABLE);
  stepper.setMaxSpeed(MOTOR_SPEED_MAX);
  stepper.setAcceleration(MOTOR_ACCELERATION);
  stepper.enableOutputs();



  pinMode(PIN_MOTOR_ZERO, INPUT_PULLUP);
  attachInterrupt(PIN_MOTOR_ZERO, motor_interrupt_handler, FALLING);

  pinMode(PIN_DIM_PWM, OUTPUT);
  attachInterrupt(PIN_DIM_ZERO_CROSS, dimmer_interrupt_handler, RISING);
  Timer1.initialize(DIM_FREQ_STEP);
  Timer1.attachInterrupt(dimmer_handler, DIM_FREQ_STEP);

  delay(2000);
  Serial.print("Steps per Round: ");
  Serial.print(STEPS_PER_ROUND);
  Serial.print(" / RPM: ");
  Serial.print(rpm);
  Serial.print(" / Steps per minute: ");
  Serial.print(motor_steps_per_minute);
  Serial.print(" / Steps per second: ");
  Serial.print(motor_steps_per_second);
  Serial.println();
    stepper.setSpeed(motor_steps_per_second);
    stepper.setMaxSpeed(motor_steps_per_second);
}

void loop() {

  if (millis() % 1000 == 0 && millis() != current_millis) {
    current_millis = millis();
    int current_pos = stepper.currentPosition();
    Serial.print(current_pos - last_pos);
    Serial.print(" / ");
    Serial.print(motor_steps_per_second);
    Serial.println();
    last_pos = current_pos;
  }
  stepper.runSpeed();

  //Serial.print(stepper.currentPosition());
//  Serial.print(" / ");
  //Serial.print(motor_steps_per_second);
  //Serial.println();

}

// I2C Communication
void i2c_receive(int byteCount){
  while(Wire.available()) {
    // FIXME read integer not just byte
    motor_steps_per_second = Wire.read();
    if (DEBUG) {
        // Serial.println(motor_steps_per_second);
    }
  }
}

// Motor
void motor_interrupt_handler()
{
  motor_is_homed = true;
  //stepper.setCurrentPosition(0);

}

// Dimmer
void dimmer_interrupt_handler() {
  dim_zero_cross = true;
  dim_counter = 0;
  digitalWriteFast(PIN_DIM_PWM, LOW);
}
void dimmer_handler() {
  // Turning on Triac between two zero-crossings after proportional time
  // regarding to dim level: 0, we turn it on right away, 128, we won't turn it on at all
  // This function gets called 128 times between each crossing
  if(dim_zero_cross == true) {
    if(dim_counter >= dim_value) {
      digitalWriteFast(PIN_DIM_PWM, HIGH);
    }
    dim_counter++;
  }
}
