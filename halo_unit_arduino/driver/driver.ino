// © 2021 Tobias Zehntner
// License AGPL-3.0 or later (https://www.gnu.org/licenses/agpl.html).

// Libraries
#include "Wire.h"
#include "digitalWriteFast.h"
#include "EveryTimerB.h"
#define Timer1 TimerB2
// Pins
#define PIN_DIM_ZERO_CROSS 2
#define PIN_DIM_PWM  4
#define PIN_MOTOR_ENABLE 5
#define PIN_MOTOR_DIR 6
#define PIN_MOTOR_PULSE 7
#define PIN_MOTOR_ZERO 8

// Hardcoded Settings
#define I2C_ARDUINO_ADDRESS 0x04
#define MOTOR_PULSE_WIDTH_MICROS 3  // Driver requires minimum 2.5
const int DIM_FREQ_STEP = 78; // EU 50Hz: 78; US 60Hz: 65

// WARNING: below values must be same in python settings and motor driver
const unsigned long MOTOR_STEPS_PER_ROUND = 10000;
const unsigned long SERIAL_BAUD_RATE = 500000;

// Global variables
// Dimmer
byte dim_value = 128;
volatile int dim_counter = 0;
volatile bool is_dim_zero_cross = false;

// Motor
int is_motor_enable = 0;
int IS_MOTOR_DIR_CCW = 0;
int is_motor_homed = 0;
volatile unsigned int motor_current_pos = 0;
unsigned long motor_measure_last_pos = 0;
unsigned long motor_last_pos_micros = 0;
unsigned long motor_micros_since_last_step = 0;
unsigned long motor_micros_between_steps;
unsigned long motor_steps_per_second_real;
unsigned long motor_measure_micros_last = 0;

// Communication
unsigned long comm_micros_last = 0;

// Timing
unsigned long current_micros = 0;


// Main
void setup() {
  comm_setup();
  motor_setup();
  dimmer_setup();

  delay(2000);
}

void loop() {
  current_micros = micros();
  motor_run();
  motor_measure_steps();
  comm_receive();
  if (current_micros - comm_micros_last > 10000000) {
    // If no commands from Pi for 10s, shut down light and motor
    shut_down();
  }
}

void shut_down() {
  dim_value = 128;
  motor_micros_between_steps = 0;
  motor_current_pos = 0;
  motor_steps_per_second_real = 0;
  motor_micros_since_last_step = 0;
}

// Communication
void comm_setup() {
  Serial.begin(SERIAL_BAUD_RATE);
}
void comm_receive() {
  if (Serial.available() > 0) {
    String command_str = Serial.readStringUntil('/');
    String data_str = Serial.readStringUntil('\n');
    int command = command_str.toInt();
    long value = data_str.toInt();
    comm_data_handler(command, value);
    comm_micros_last = current_micros;
  }
}
void comm_data_handler(int command, long value) {
  switch (command) {
    // Assign new values, confirm reception
    case 1:
      dim_value = value;
      Serial.println((String)command + "/");
      break;
    case 2:
      if (value >= 300) {
        // Cap motor speed at max 20rpm for safety
        motor_micros_between_steps = value;
      }
      Serial.println((String)command + "/");
      break;
    // Respond with values
    case 3:
      Serial.println((String)command + "/" + motor_current_pos + "," + motor_steps_per_second_real);
      break;
    case 9:
      shut_down();
      Serial.println((String)command + "/");
      break;
    default:
      break;
  }
}

// Motor
void motor_setup() {
  pinMode(PIN_MOTOR_ENABLE, OUTPUT);
  pinMode(PIN_MOTOR_DIR, OUTPUT);
  pinMode(PIN_MOTOR_PULSE, OUTPUT);
  pinMode(PIN_MOTOR_ZERO, INPUT_PULLUP);
  attachInterrupt(PIN_MOTOR_ZERO, motor_interrupt_handler, FALLING);

  digitalWrite(PIN_MOTOR_ENABLE, is_motor_enable);
  digitalWrite(PIN_MOTOR_DIR, IS_MOTOR_DIR_CCW);
}
void motor_interrupt_handler() {
  motor_current_pos = 0;
}
void motor_run() {
  motor_micros_since_last_step = current_micros - motor_last_pos_micros;
  if (motor_micros_between_steps > 0) {
    if (!is_motor_enable) {
      is_motor_enable = true;
      digitalWrite(PIN_MOTOR_ENABLE, is_motor_enable);
    }
    if (motor_micros_since_last_step >= motor_micros_between_steps) {
      digitalWrite(PIN_MOTOR_PULSE, HIGH);
      delayMicroseconds(MOTOR_PULSE_WIDTH_MICROS);
      digitalWrite(PIN_MOTOR_PULSE, LOW);

      if (IS_MOTOR_DIR_CCW) {
        if (motor_current_pos == MOTOR_STEPS_PER_ROUND) {
          motor_current_pos = 0;
        }
        motor_current_pos++;
      } else {
        if (motor_current_pos == 0) {
          motor_current_pos = MOTOR_STEPS_PER_ROUND;
        }
        motor_current_pos--;
      }
      motor_last_pos_micros = current_micros;
      }
  } else if (is_motor_enable) {
    is_motor_enable = false;
    digitalWrite(PIN_MOTOR_ENABLE, is_motor_enable);
  }
}

void motor_measure_steps() {
  unsigned long steps_since_last;
  unsigned long micros_since_last = current_micros - motor_measure_micros_last;
  if (micros_since_last >= 1000000) {
    // Measure real steps per second
    if (IS_MOTOR_DIR_CCW) {
      // Counter-clockwise
      if (motor_current_pos < motor_measure_last_pos) {
        // Motor has crossed zero point
        steps_since_last = (motor_current_pos + MOTOR_STEPS_PER_ROUND) - motor_measure_last_pos;
      } else {
        steps_since_last = motor_current_pos - motor_measure_last_pos;
      }
    } else {
      // Clockwise
      if (motor_current_pos > motor_measure_last_pos) {
        // Motor has crossed zero point
        steps_since_last = (motor_measure_last_pos + MOTOR_STEPS_PER_ROUND) - motor_current_pos;
      } else {
        steps_since_last = motor_measure_last_pos - motor_current_pos;
      }
    }
    motor_steps_per_second_real = steps_since_last * 1000000 / micros_since_last;
    motor_measure_last_pos = motor_current_pos;
    motor_measure_micros_last = current_micros;
  }
}


// Dimmer
void dimmer_setup() {
  pinMode(PIN_DIM_PWM, OUTPUT);
  pinMode(PIN_DIM_ZERO_CROSS, INPUT);
  attachInterrupt(PIN_DIM_ZERO_CROSS, dimmer_interrupt_handler, RISING);
  Timer1.initialize();
  Timer1.attachInterrupt(dimmer_handler, DIM_FREQ_STEP);
  Timer1.setPeriod(DIM_FREQ_STEP);
}
void dimmer_interrupt_handler() {
  is_dim_zero_cross = true;
  dim_counter = 0;
  digitalWriteFast(PIN_DIM_PWM, LOW);
}
void dimmer_handler() {
  if(is_dim_zero_cross == true) {
    if(dim_counter >= dim_value) {
      digitalWriteFast(PIN_DIM_PWM, HIGH);
    }
    dim_counter++;
  }
}
