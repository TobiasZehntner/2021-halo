# © 2021 Tobias Zehntner
# License AGPL-3.0 or later (https://www.gnu.org/licenses/agpl.html).

import logging

NUM_UNITS = 7

units = {}
for i in range(1, NUM_UNITS + 1):
    units[i] = {"hostname": f"halo-unit-{i}.local", "name": f"Halo Unit {i}"}

# Individual Unit data
# Grid 4.20 x 4.20

units[1].update(
    {
        "motor_zero_offset_degrees": 180,
        "grid_pos_x": 3.92,  # grid position in meters
        "grid_pos_y": 16.60,
        "cable_length_m": 2.34,  # required to be above 2.0 to avoid math error
        "target_radius": 1.67,
    }
)
units[2].update(
    {
        "motor_zero_offset_degrees": 180,
        "grid_pos_x": 4.82,
        "grid_pos_y": 13.22,
        "cable_length_m": 2.32,
        "target_radius": 1.20,
    }
)
units[3].update(
    {
        "motor_zero_offset_degrees": 90,
        "grid_pos_x": 2.94,
        "grid_pos_y": 9.93,
        "cable_length_m": 5.76,
        "target_radius": 1.50,
    }
)
units[4].update(
    {
        "motor_zero_offset_degrees": 90,
        "grid_pos_x": 5.91,
        "grid_pos_y": 8.37,
        "cable_length_m": 5.31,
        "target_radius": 0.91,
    }
)
units[5].update(
    {
        "motor_zero_offset_degrees": 90,
        "grid_pos_x": 1.84,
        "grid_pos_y": 7.02,
        "cable_length_m": 5.36,
        "target_radius": 0.90,
    }
)
units[6].update(
    {
        "motor_zero_offset_degrees": 90,
        "grid_pos_x": 1.17,
        "grid_pos_y": 4.52,
        "cable_length_m": 5.27,
        "target_radius": 1.00,
    }
)
units[7].update(
    {
        "motor_zero_offset_degrees": 90,
        "grid_pos_x": 4.99,
        "grid_pos_y": 5.02,
        "cable_length_m": 5.38,
        "target_radius": 2.04,
    }
)

SETTINGS = {
    "num_units": NUM_UNITS,
    "controller": {
        "hostnames": [
            "halo-controller.local",
            "halo-controller",
            "Tobis-MacBook-Pro.local",
        ],
        "name": "Halo Controller",
    },
    "units": units,
    "communication": {
        "other_units_update_rate_sec": 5,  # Update other unit positions every n seconds
        "own_unit_update_rate_sec": 1,  # Get position and rpm_real every n seconds
    },
    "calibration": {
        "min_dim": 128,  # Lowest brightness
        "max_dim": 60,  # Highest brightness
        "min_distance": 0.5,  # Min distance between lights in meters
        "max_distance": 6.30,  # Max distance between two closest lights in meters
        "update_light_sec": 0.05,  # Update light every n seconds
        # Seconds to compute position ahead (correct reaction time of incandescent dim)
        "compute_ahead_sec": 0.2,
    },
    "hardware": {
        "serial_baud_rate": 500000,  # WARNING: must be same on arduino
        # WARNING: must be same on arduino and driver hardware:
        "motor_driver_steps_per_round": 10000,
        "light_off": 128,  # Turn light off fully
        "light_on": 0,  # Turn light on fully
        "motor_arm_length": 0.172,
    },
    "logging": {"level": logging.DEBUG},
    "ui": {
        "fps": 20,
        "window": {"width": 600, "height": 1000},
        "draw_pos": {"x": 100, "y": 50},  # 0/0 unit_1 position within the window
        "m_to_px_ratio": 50,
        "draw_measures": {
            "bulb_radius": 0.08,
            "bulb_light_radius": 0.1,
            "throw_light_radius": 4.0,
            "trace_length": 50,
        },
    },
}
