# © 2021 Tobias Zehntner
# License AGPL-3.0 or later (https://www.gnu.org/licenses/agpl.html).

import argparse
import asyncio
import logging
import platform
import sys

from halo_controller import SETTINGS, HaloController

if platform.system() == "Darwin":
    # User Interface only to run on Mac
    allow_ui = True
    from halo_controller import UserInterface
else:
    allow_ui = False


class ArgParser(argparse.ArgumentParser):
    def error(self, message):
        sys.stderr.write("error: %s\n" % message)
        self.print_help(sys.stderr)
        sys.exit(2)


def setup_main_logging():
    FORMAT = logging.Formatter("%(asctime)s - %(levelname)s - %(message)s")
    console_handler = logging.StreamHandler()
    console_handler.setFormatter(FORMAT)
    file_handler = logging.FileHandler("logs/controller.log", mode="a")
    file_handler.setFormatter(FORMAT)
    logger.setLevel(SETTINGS["logging"]["level"])
    logger.addHandler(console_handler)
    logger.addHandler(file_handler)


logger = logging.getLogger("SYSTEM")

parser = ArgParser(description="Run Halo Controller")
parser.add_argument("controller_hostname")
parser.add_argument(
    "-ui", "--user-interface", action="store_true", help="enable user interface"
)
args = parser.parse_args()
controller = HaloController(args.controller_hostname)

if args.user_interface:
    if allow_ui:
        controller.ui = UserInterface(controller)
    else:
        logger.warning("User Interface flag ignored. Cannot run on RPi.")


async def setup():
    logger.info(f"Starting {controller.name}...")
    setup_main_logging()
    await controller.setup()


async def run():
    await setup()
    await asyncio.gather(controller.run())


async def shut_down():
    logger.info("Shutting down...")
    await controller.shut_down()


async def main():
    try:
        await asyncio.gather(run())
    finally:
        await shut_down()
        logger.info(f"{controller.name} turned off.")


if __name__ == "__main__":
    try:
        asyncio.run(main())
    except KeyboardInterrupt:
        pass
