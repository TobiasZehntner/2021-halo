import asyncio
import random

q = asyncio.Queue()


async def handle_echo(reader, writer):
    data = await reader.read(100)
    message = data.decode()
    addr = writer.get_extra_info('peername')

    print(f"Received {message!r} from {addr!r}")

    print(f"Send: {message!r}")
    writer.write(data)
    await writer.drain()

    print("Close the connection")
    writer.close()


async def run_server_async():
    server = await asyncio.start_server(
        handle_echo, '127.0.0.1', 8888)

    addr = server.sockets[0].getsockname()
    print(f'Serving on {addr}')

    async with server:
        await server.serve_forever()

async def count():
    n = 0
    while True:
        n += 1
        print(n)
        await asyncio.sleep(1)



async def main():

    asyncio.create_task(run_server_async())
    asyncio.create_task(count())
    await asyncio.Event().wait()




if __name__ == "__main__":
    asyncio.run(main())

