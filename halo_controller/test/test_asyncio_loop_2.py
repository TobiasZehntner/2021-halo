
import signal
import asyncio
import logging
import random

logging.basicConfig(
    level=logging.INFO,
    format="%(asctime)s,%(msecs)d %(levelname)s: %(message)s",
    datefmt="%H:%M:%S",
)


async def task_3(queue):
    print('task 3')
    while True:
        event = asyncio.Event()
        wait_time = random.uniform(0.1, 1.0)
        print(f"queueing: {wait_time}")
        await queue.put((wait_time, event))
        await event.wait()


async def task_1():
    await asyncio.sleep(1000)


async def task_2():
    print('run error task 2')
    await asyncio.sleep(2)
    print('raising error...')
    raise RuntimeError('something went wrong')


async def runner(queue):
    while True:
        try:
            wait_time, event = await queue.get()
            print(f"processing: {wait_time}")
            await asyncio.sleep(wait_time)
            event.set()
        except asyncio.CancelledError:
            print("runner got cancelled")
            raise


async def clean_up(queue, runner_task):
    print('cleaning up...')
    event = asyncio.Event()
    await queue.put((1, event))
    await event.wait()
    runner_task.cancel()
    print("clean up finished")


def handle_exception(loop, context):
    # context["message"] will always be there; but context["exception"] may not
    msg = context.get("exception", context["message"])
    logging.error(f"Caught exception: {msg}")
    logging.info("Shutting down...")
    asyncio.create_task(shutdown(loop))


async def shutdown(loop, signal=None):
    """Cleanup tasks tied to the service's shutdown."""
    if signal:
        logging.info(f"Received exit signal {signal.name}...")
    logging.info("Closing database connections")
    logging.info("Nacking outstanding messages")
    tasks = [t for t in asyncio.all_tasks() if t is not
             asyncio.current_task()]

    [task.cancel() for task in tasks]

    logging.info(f"Cancelling {len(tasks)} outstanding tasks")
    await asyncio.gather(*tasks, return_exceptions=True)
    logging.info(f"Flushing metrics")
    loop.stop()


def main():
    loop = asyncio.get_event_loop()
    # May want to catch other signals too
    signals = (signal.SIGHUP, signal.SIGTERM, signal.SIGINT)
    for s in signals:
        loop.add_signal_handler(
            s, lambda s=s: asyncio.create_task(shutdown(loop, signal=s)))
    # comment out the line below to see how unhandled exceptions behave
    loop.set_exception_handler(handle_exception)
    queue = asyncio.Queue()

    try:
        loop.create_task(task_1())
        loop.create_task(task_2())
        loop.create_task(task_3(queue))
        loop.create_task(runner(queue))
        loop.run_forever()
    finally:
        runner_task = loop.create_task(runner(queue))
        loop.run_until_complete(clean_up(queue, runner_task))
        loop.close()
        logging.info("Successfully shutdown the Mayhem service.")


if __name__ == "__main__":
    main()
