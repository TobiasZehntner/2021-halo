

import asyncio
import logging
import random
queue = asyncio.Queue()

async def task_3():
    print('task 3')
    while True:
        event = asyncio.Event()
        wait_time = random.uniform(0.1, 1.0)
        print(f"queueing: {wait_time}")
        await queue.put((wait_time, event))
        await event.wait()

async def task_1():
    print('call task 3 from task 1')
    await task_3()
    await asyncio.sleep(1000)


async def task_2():
    print('run error task 2')
    await asyncio.sleep(2)
    print('raising error...')
    raise RuntimeError('something went wrong')


async def runner():
    while True:
        try:
            wait_time, event = await queue.get()
            print(f"processing: {wait_time}")
            await asyncio.sleep(wait_time)
            event.set()
        except asyncio.CancelledError:
            print("runner got cancelled")
            raise

async def clean_up(runner_task):
    print('cleaning up...')
    event = asyncio.Event()
    await queue.put((1, event))
    await event.wait()
    runner_task.cancel()
    print("clean up finished")


async def main():
    loop = asyncio.get_event_loop()
    try:
        await asyncio.gather(task_1(), task_2(), task_3(), runner(), return_exceptions=True)
    finally:
        runner_task = asyncio.create_task(runner())
        await asyncio.gather(clean_up(runner_task))
        print('done')


if __name__ == "__main__":
    try:
        asyncio.run(main())
    except KeyboardInterrupt:
        pass
