# © 2021 Tobias Zehntner
# License AGPL-3.0 or later (https://www.gnu.org/licenses/agpl.html).


class Base:
    def __init__(self):
        from __main__ import logger

        self.logger = logger
