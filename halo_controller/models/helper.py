# © 2021 Tobias Zehntner
# License AGPL-3.0 or later (https://www.gnu.org/licenses/agpl.html).

import math
from datetime import datetime

from dateutil import parser

from halo_controller import SETTINGS


def map_range(x, in_min, in_max, out_min, out_max, clamp=False):
    """
    Arduino's map() function: Re-maps a number from one range to another
    :param clamp: clamping result to the out range
    """
    res = (x - in_min) * (out_max - out_min) / (in_max - in_min) + out_min
    if clamp:
        res = max(min(max(out_min, out_max), res), min(out_min, out_max))
    return res


def get_distance_between_points(x1y1, x2y2):
    """
    Return distance between two points
    :param x1y1: (x_pos1, y_pos1)
    :type x1y1: tuple
    :param x2y2: (x_pos2, y_pos2)
    :type x2y2: tuple
    :return:
    :rtype: float
    """
    a = abs(x1y1[0] - x2y2[0])
    b = abs(x1y1[1] - x2y2[1])
    return math.sqrt((a ** 2) + (b ** 2))


def get_relative_position(motor_angle, radius):
    """
                      o (x,y)
                     /|
            radius  / |
                   /  | y
                  O___|
                    x

    Get position of bulb relative to its own center

    :param grid_pos_x: Unit x grid position in meters
    :type grid_pos_x: float
    :param grid_pos_y: Unit y grid position in meters
    :type grid_pos_y: float
    :param motor_angle: Angle of motor
    :type motor_angle: float
    :param radius: Radius of bulb
    :type radius: float
    """
    x = radius * math.cos(math.radians(motor_angle))
    y = radius * math.sin(math.radians(motor_angle))
    return x, y


def get_radius(cable_length, radians, motor_arm_length):
    """
                      o
                 c1  /| angle
                    / |
                   o--o Motor arm length
         c2       /|  |
   cable length  / |  |
                /  |  |
               O___|__|
                a1
        a1 + motor_arm = radius


    :return: Radius
    :rtype: float
    :param cable_length: cable length in meters
    :type cable_length: float
    :param radians: angle in radians
    :type radians: float
    :param motor_arm_length: motor arm length in meters
    :type motor_arm_length: float
    """
    if radians == 0:
        return motor_arm_length
    a1 = math.sin(radians) * cable_length
    radius = a1 + motor_arm_length
    return radius


def get_angle(cable_length, radius, motor_arm_length):
    """
                      o
                 c1  /| angle
                    / |
                   o--o Motor arm length
         c2       /|  |
   cable length  / |  |
                /  |  |
               O___|__|
                radius
                a1  motor_arm_length


    :param cable_length: cable length in meters
    :type cable_length: float
    :param radius: radius in meters
    :type radius: float
    :param motor_arm_length: motor arm length in meters
    :type motor_arm_length: float
    """
    a1 = radius - motor_arm_length
    angle = math.degrees(math.asin(a1 / cable_length))
    return angle


def get_rpm(cable_length, angle, motor_arm_length):
    """
    Get rpm speed of a conical pendulum from the angle of the cable

                      o
                 c1  /| angle
                    / |
                   o--o Motor arm length
         c2       /   |
    cable length /    |
                /     |
               O______|
                radius

    :param cable_length: cable length in meters
    :type cable_length: float
    :param angle: angle in degrees
    :type angle: float
    :param motor_arm_length: motor arm length in meters
    :type motor_arm_length: float
    :return: speed in rounds per minute
    :rtype: float
    """
    if angle <= 0:
        return 0
    c1 = motor_arm_length / math.sin(math.radians(angle))
    theoretical_length = cable_length + c1
    rpm = (
        (math.sqrt(9.80665 / (theoretical_length * math.cos(math.radians(angle)))))
        / (2 * math.pi)
    ) * 60
    return rpm


def rpm_to_radians_per_second(rpm, is_dir_ccw):
    """
    Convert rounds per minute to radians per second
    :param is_dir_ccw: True if direction is counter-clockwise
    :type is_dir_ccw: bool
    :return: Radians per Second
    :rtype: float
    :param rpm: Rounds per minute
    :type rpm: float
    """
    radians_per_second = 2 * math.pi / 60 * rpm
    if not is_dir_ccw:
        radians_per_second *= -1
    return radians_per_second


def radians_per_second_to_rpm(radians_per_second, is_dir_ccw):
    """
    Convert radians per second to rounds per minute
    :param is_dir_ccw: True if direction is counter-clockwise
    :type is_dir_ccw: bool
    :return: rpm
    :rtype: float
    :param radians_per_second: Radians per second
    :type radians_per_second: float
    """
    rpm = radians_per_second / 2 * math.pi * 6
    if not is_dir_ccw:
        rpm *= -1
    return rpm


def rotate_vector(x1y1, radians):
    """

    :param x1:
    :type x1:
    :param y1:
    :type y1:
    :param radians:
    :type radians: float
    :return: New position after rotation: x2, y2
    :rtype: tuple
    """
    x2 = (x1y1[0] * math.cos(radians)) - (x1y1[1] * math.sin(radians))
    y2 = (x1y1[0] * math.sin(radians)) + (x1y1[1] * math.cos(radians))
    return x2, y2


def rpm_to_micros(rpm):
    steps_per_round = SETTINGS["hardware"]["motor_driver_steps_per_round"]
    steps_per_second = rpm * steps_per_round / 60
    micros_between_steps = int(1000000 / steps_per_second)
    return micros_between_steps


def steps_to_rpm(steps_per_second):
    steps_per_round = SETTINGS["hardware"]["motor_driver_steps_per_round"]
    rpm = steps_per_second * 60 / steps_per_round
    return rpm


def compute_current_positions(position_data, compute_ahead=False):
    current_positions = {}
    for unit_id in position_data.keys():
        if (
            not position_data[unit_id]["is_connected"]
            or not position_data[unit_id]["data"]
        ):
            continue
        unit_data = position_data[unit_id]["data"]
        grid_pos_x = SETTINGS["units"][unit_id]["grid_pos_x"]
        grid_pos_y = SETTINGS["units"][unit_id]["grid_pos_y"]
        current_time = datetime.now()
        delta_time = current_time - parser.isoparse(unit_data["timestamp"])
        seconds = delta_time.total_seconds()
        if compute_ahead:
            seconds += SETTINGS["calibration"]["compute_ahead_sec"]
        rotation_radians = unit_data["radians_per_second"] * seconds
        pos_relative = rotate_vector(unit_data["pos_relative"], rotation_radians)

        pos_absolute_x = pos_relative[0] + grid_pos_x
        pos_absolute_y = pos_relative[1] + grid_pos_y
        current_positions[unit_id] = (pos_absolute_x, pos_absolute_y)
    return current_positions


def m_to_px(m):
    ratio = SETTINGS["ui"]["m_to_px_ratio"]
    return m * ratio
