import asyncio

import pyglet

from halo_controller import SETTINGS, Base, helper

UNIT_PX_POSITIONS = {
    unit_id: (
        helper.m_to_px(SETTINGS["units"][unit_id]["grid_pos_x"]),
        helper.m_to_px(SETTINGS["units"][unit_id]["grid_pos_y"]),
    )
    for unit_id in SETTINGS["units"].keys()
}


class UiWindow(pyglet.window.Window):
    def __init__(self, width, height, ui):
        super().__init__()
        self.ui = ui
        self.width = width
        self.height = height


class UserInterface(Base):
    def __init__(self, controller):
        super().__init__()
        self.controller = controller
        self.window = UiWindow(
            SETTINGS["ui"]["window"]["width"], SETTINGS["ui"]["window"]["height"], self
        )
        self.pos_px = SETTINGS["ui"]["draw_pos"]
        self.fps_display = pyglet.window.FPSDisplay(window=self.window)

        self.gl_groups = {
            "background": pyglet.graphics.OrderedGroup(0),
            "middleground": pyglet.graphics.OrderedGroup(1),
            "foreground": pyglet.graphics.OrderedGroup(2),
            "interface": pyglet.graphics.OrderedGroup(3),
        }
        self.units = UserInterfaceGroup()
        self.debug = UserInterfaceGroup()
        self.interface = UserInterfaceGroup()

    async def run(self):
        dt = 1 / SETTINGS["ui"]["fps"]
        while True:
            pyglet.clock.tick()

            for window in pyglet.app.windows:
                window.switch_to()
                window.dispatch_events()
                self.update_ui()
                window.flip()

            await asyncio.sleep(dt)

    def update_ui(self):
        self.update()
        self.draw()

    def update(self):
        positions = helper.compute_current_positions(self.controller.position_data)
        for unit_id in positions.keys():
            endpoint = positions[unit_id]

            center_px_x = UNIT_PX_POSITIONS[unit_id][0]
            center_px_y = UNIT_PX_POSITIONS[unit_id][1]
            endpoint_px_x = helper.m_to_px(endpoint[0])
            endpoint_px_y = helper.m_to_px(endpoint[1])

            if not self.units.elements.get(unit_id):
                self.init_draw_unit(
                    unit_id, center_px_x, center_px_y, endpoint_px_x, endpoint_px_y
                )
            self.units.elements[unit_id]["cable"].vertices = [
                center_px_x,
                center_px_y,
                endpoint_px_x,
                endpoint_px_y,
            ]

            trace_vertices = list(
                self.units.elements[unit_id]["trajectory_trace"].vertices
            )
            trace_vertices[-2:] = []
            trace_vertices.insert(0, endpoint_px_x)
            trace_vertices.insert(1, endpoint_px_y)
            self.units.elements[unit_id]["trajectory_trace"].vertices = trace_vertices

            # TODO is_dir_ccw should not be hardcoded but taken from settings
            rpm = "%.2f" % self.controller.position_data[unit_id]["data"]["rpm"]
            self.units.elements[unit_id]["rpm_label"].text = rpm + "rpm"

    def init_draw_unit(
        self, unit_id, center_px_x, center_px_y, endpoint_px_x, endpoint_px_y
    ):
        self.units.elements[unit_id] = {}
        cable = self.units.batch.add(
            2,
            pyglet.gl.GL_LINES,
            self.gl_groups["foreground"],
            ("v2f/stream", (center_px_x, center_px_y, endpoint_px_x, endpoint_px_y)),
            ("c4B/static", (255, 255, 255, 255) * 2),
        )
        self.units.elements[unit_id]["cable"] = cable

        trace_length = SETTINGS["ui"]["draw_measures"]["trace_length"]
        trace_colors = []
        for i in range(trace_length, 0, -1):
            trace_colors += 255, 255, 255, i
        trajectory_trace = self.units.batch.add(
            trace_length,
            pyglet.gl.GL_LINES,
            self.gl_groups["interface"],
            ("v2f/stream", (endpoint_px_x, endpoint_px_y) * trace_length),
            ("c4B/static", trace_colors),
        )
        self.units.elements[unit_id]["trajectory_trace"] = trajectory_trace

        unit_label = pyglet.text.Label(
            str(unit_id),
            font_name="Helvetica",
            font_size=12,
            x=center_px_x,
            y=center_px_y + 20,
            anchor_x="center",
            anchor_y="center",
            color=(255, 255, 255, 255),
            batch=self.units.batch,
            group=self.gl_groups["interface"],
        )
        self.units.elements[unit_id]["unit_label"] = unit_label

        rpm_label = pyglet.text.Label(
            str(0),
            font_name="Helvetica",
            font_size=10,
            x=center_px_x,
            y=center_px_y - 20,
            anchor_x="center",
            anchor_y="center",
            color=(255, 255, 255, 180),
            batch=self.units.batch,
            group=self.gl_groups["interface"],
        )
        self.units.elements[unit_id]["rpm_label"] = rpm_label

    def draw(self):
        pyglet.gl.glClear(pyglet.gl.GL_COLOR_BUFFER_BIT)
        pyglet.gl.glLoadIdentity()
        pyglet.gl.glPushMatrix()
        pyglet.gl.glTranslatef(self.pos_px["x"], self.pos_px["y"], 0.0)
        pyglet.gl.glEnable(pyglet.gl.GL_BLEND)
        pyglet.gl.glBlendFunc(pyglet.gl.GL_SRC_ALPHA, pyglet.gl.GL_ONE_MINUS_SRC_ALPHA)
        self.units.batch.draw()
        if self.debug.is_visible:
            self.debug.batch.draw()
        if self.interface.is_visible:
            self.interface.batch.draw()
            self.fps_display.draw()

        pyglet.gl.glPopMatrix()


class UserInterfaceGroup(Base):
    def __init__(self):
        super().__init__()
        self.is_visible = True
        self.batch = pyglet.graphics.Batch()
        self.elements = {}
