# © 2021 Tobias Zehntner
# License AGPL-3.0 or later (https://www.gnu.org/licenses/agpl.html).

import asyncio
import json
from datetime import datetime

from dateutil import parser

from halo_controller import SETTINGS

from .base import Base


class HaloController(Base):
    def __init__(self, hostname):
        super().__init__()
        self.name = SETTINGS["controller"]["name"]
        self.server = None
        self.ui = None
        self.position_data = {}
        self.hostname = hostname

    async def shut_down(self):
        """
        Clean up
        """
        if self.server:
            self.server.close()
            self.logger.debug("Closing Server...")
            await self.server.wait_closed()

    def register_unit(self, unit_id):
        if self.position_data.get(unit_id, {}).get("is_connected"):
            self.logger.warning(f"Unit {unit_id} re-connected. ")
        self.position_data.update({unit_id: {"is_connected": True, "data": {}}})
        self.logger.info(
            f"Unit {unit_id} connected "
            f"({len([k for k, v in self.position_data.items() if v['is_connected']])}/{SETTINGS['num_units']})."
        )

    async def handle_request(self, reader, writer):
        data_in = await reader.readuntil(b"\n")
        values = json.loads(data_in.decode())
        command = values["command"]

        unit_id = values["unit_id"]
        if command == "connect":
            self.register_unit(unit_id)
            response = {"status": "ok"}
        elif command == "post_data":
            if (
                not self.position_data.get(unit_id)
                or not self.position_data[unit_id]["is_connected"]
            ):
                self.register_unit(unit_id)

            self.position_data[unit_id]["data"].update(values["data"])
            response = {"status": "ok", "data": self.position_data}
            self.logger.debug(f"Exchanged data with Unit {unit_id}")
        else:
            response = {"status": "error", "msg": "Command not recognized"}
            self.logger.error(f"Command not recognized: {command}")
        data_out = (json.dumps(response) + "\n").encode()
        writer.write(data_out)
        await writer.drain()
        writer.close()
        await writer.wait_closed()

    async def server_start(self):
        self.server = await asyncio.start_server(
            self.handle_request, self.hostname, 8888
        )
        addr = self.server.sockets[0].getsockname()
        self.logger.info(f"Serving on {self.hostname} {addr}")

    async def server_run(self):
        async with self.server:
            await self.server.serve_forever()

    async def setup(self):
        await self.server_start()

    async def run(self):
        tasks = [self.server_run(), self.check_units_alive()]
        if self.ui:
            tasks.append(self.ui.run())
        await asyncio.gather(*tasks)

    async def check_units_alive(self):
        while True:
            current_time = datetime.now()
            expected_update_sec = SETTINGS["communication"][
                "other_units_update_rate_sec"
            ]
            check_units_alive_secs = 3 * expected_update_sec
            for unit_id in self.position_data.keys():
                unit_data = self.position_data[unit_id]["data"]
                if unit_data:
                    delta_time = current_time - parser.isoparse(unit_data["timestamp"])
                    if delta_time.total_seconds() > check_units_alive_secs:
                        self.position_data[unit_id]["is_connected"] = False
                        self.logger.warning(
                            f"Have not received new data from Unit {unit_id} for "
                            f"{str(delta_time).split('.')[0]}"
                        )
            await asyncio.sleep(check_units_alive_secs)
