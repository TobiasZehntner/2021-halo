# Halo

Code base for Halo (2021)

## Install
1. ` sudo -H pip install pipenv`

## Setup
### Mac
#### Local Host
1. Add controller host name to the hosts file:
    ```bash
    sudo nano /etc/hosts
    ```
2. Add the following line to the file, using the same hostname than in the controller settings:
    ```bash
    # Halo Project
    127.0.0.1 halo-controller.local
    ```
3. Flush Mac's DNS cache
    ```bash
    sudo killall -HUP mDNSResponder
    ```

### RPi
#### Install raspian
- Download latest image: [Raspbian](https://www.raspberrypi.org/downloads/raspbian/)
- Insert MicroSD card into Mac
- Terminal: `diskutil list`
- Check SD name (e.g. `disk2`) from the list
- Unmount the disk: `diskutil unmountDisk /dev/disk2`
- Copy image `sudo dd bs=1m if=path_of_your_image.img of=/dev/rdisk2; sync`
- Eject image `sudo diskutil eject /dev/rdisk2`
- Re-insert card to mac
- Add the file in halo/pi/wpa_supplicant.conf to pi/boot/
- Add an empty file called 'ssh' to enable ssh `touch /Volumes/boot/ssh`
- Insert card to Pi and start

More info [here](https://desertbot.io/blog/headless-raspberry-pi-3-bplus-ssh-wifi-setup).

#### Backup SD card
```bash
sudo dd if=/dev/rdisk2 of=/Users/Yourname/Desktop/pi.img bs=1m
```

#### Setup
- `ssh pi@raspberrypi.local`
- Login with default password `raspberry`
- Change password with `passwd`
- Change hostname via `sudo raspi-config` > System Options > Hostname to `halo-unit-<id>`
- Enable Interfacing Options > Serial: login shell: No, port hardware: Yes
- Update system: `sudo apt-get update; sudo apt update; sudo apt full-upgrade;`
- `sudo nano /etc/default/locale` change it to
```bash
LANG=en_US.UTF-8
LC_ALL=en_US.UTF-8
LANGUAGE=en_US.UTF-8
```
- Reboot
- When disconnected, from mac, copy ssh key: `ssh-copy-id pi@halo-unit-1.local`

##### Add multiple wifi networks
/!\ This has to be done from the pi. Adding multiple networks to the wpa_supplicant file on boot does not work!

- Search for available networks (only 2.4ghz on RPi Zero W:
   `sudo iwlist wlan0 scan | grep ESSID`
- Add them to file: `sudo nano /etc/wpa_supplicant/wpa_supplicant.conf`
- By priority:
```
ctrl_interface=DIR=/var/run/wpa_supplicant GROUP=netdev
update_config=1
country=BE

network={
  ssid="<wifi_name>"
  psk="<wifi_password>"
  priority= 1
}

network={
  ssid="wifi_name"
  psk="wifi_password"
  priority= 2
}
```

#### Dependencies
- Add to bash file `sudo nano ~.bashrc`:
```bash
export PIPENV_TIMEOUT=50000
export PIPENV_INSTALL_TIMEOUT=50000
```
- Make python3 standard: `sudo update-alternatives --install /usr/bin/python python /usr/bin/python3 2`
- `sudo apt install git`
- `sudo apt-get install python3-pip`
- `pip3 install --user pipenv`
- `git clone https://gitlab.com/TobiasZehntner/2021-halo.git halo`
- `cd ~/halo`
- `mkdir logs`
- `PIP_REQUIRE_VIRTUALENV=false pip3 install certifi --upgrade`
- `pipenv install`

##### Arduino-cli
Be able to upload code to Arduino from RPi. -> [How To](https://siytek.com/arduino-cli-raspberry-pi/)

1. Install
    ```bash
    curl -fsSL https://raw.githubusercontent.com/arduino/arduino-cli/master/install.sh | sh
    ```
2. Add path `export PATH=$PATH:/home/pi/bin` in `~.bashrc`
3. Create config file: `https://siytek.com/arduino-cli-raspberry-pi/`
4. Update index: `arduino-cli core update-index`
5. Find available boards: `arduino-cli board list` and get the details:
    ```bash
    Port         Protocol Type              Board Name         FQBN                     Core
    /dev/ttyACM0 serial   Serial Port (USB) Arduino Nano Every arduino:megaavr:nona4809 arduino:megaavr
   ```
6. Install platform: `arduino-cli core install arduino:megaavr`
8. Copy libraries from Mac:
    ```bash
   scp -r .../Arduino/libraries/digitalWriteFast pi@halo-unit-master.local:/home/pi/Arduino/libraries
   scp -r .../Arduino/libraries/EveryTimerB pi@halo-unit-master.local:/home/pi/Arduino/libraries
   ```
   If compile fails due to relative header path, the library file might have to be changed to absolute path
   `/home/pi/Arduino/libraries/EveryTimerB/EveryTimerB.h:86`:
   ```
   #include "/home/pi/Arduino/libraries/EveryTimerB/MegaAvr20Mhz.h"
   ```

9. Update index: `arduino-cli core update-index`
10. Compile: `arduino-cli compile --fqbn arduino:megaavr:nona4809 ~/halo/halo_unit_arduino/driver`
11. Upload: `arduino-cli upload -p /dev/ttyACM0 --fqbn arduino:megaavr:nona4809 ~/halo/halo_unit_arduino/driver`

In one command:
```bash
arduino-cli compile --fqbn arduino:megaavr:nona4809 ~/halo/halo_unit_arduino/driver;arduino-cli upload -p /dev/ttyACM0 --fqbn arduino:megaavr:nona4809 ~/halo/halo_unit_arduino/driver;
```

##### Pipenv

###### Uninstall pipenv
- Linux:
```bash
sudo apt remove pipenv
sudo apt autoclean && sudo apt autoremove
```


###### Install
```bash
$ pipenv install
```
###### Add packages
```bash
$ pipenv install <package>
```
This will add the requirement to the pipfile.

###### Remove packages
```bash
pipenv -rm <package>
```

###### Update dependencies
Check what's available:
```bash
pipenv update --outdated
```
Update all:
```bash
pipenv update
```

Update specific:
```bash
pipenv update <pkg>
```

###### Enter virtual environment
To run the environment python from the command line:
```bash
$ pipenv shell
$ which python
```
###### Exit pipenv

```bash
$ exit
```
###### Help
- [pipenv docs](https://pipenv-fork.readthedocs.io/en/latest/basics.html)
- [pipenv guide](https://realpython.com/pipenv-guide/)

#### Project setup
On Rpi:
- `git clone https://gitlab.com/TobiasZehntner/2021-halo.git ~/halo`
- `cd halo`
- `pipenv update`

On PyCharm:
1. Go to deployment > in SSH Configuration, create new one
2. Interpreter > choose SSH config, choose rpi pipenv python3
3. Configurations: create new one using the interpreter
4. Deployment: set mappings

## Run
### From Mac

#### Controller
```pipenv run python -m halo_controller -c halo-controller.local```

#### Unit
```pipenv run python -m halo_unit 1```

#### In Terminal
- ```pipenv run python -m halo_controller```
- ```pipenv run python -m halo_unit <unit_id>```

#### In Pycharm
- Script path `.../2021-halo/halo_controller/`
- Script path `.../2021-halo/halo_unit`, parameters `1`

### From Rpi
Load env.sh before running to find the module from the virtual env.

Running Unit from RPi
```bash
cd ~/halo && source ~/halo/env.sh && python3 -m pipenv run python -m halo_unit 6
```

Running Controller from RPi
```bash
cd ~/halo && source ~/halo/env.sh && python3 -m pipenv run python -m halo_controller halo-controller.local
```
If running from cron, controller hostname needs to be `0.0.0.0` (and not `127.0.1.0`) in order to be accessible:
- `sudo nano /etc/hosts`
```bash
127.0.0.1       localhost
::1             localhost ip6-localhost ip6-loopback
ff02::1         ip6-allnodes
ff02::2         ip6-allrouters

0.0.0.0 halo-controller
```

If controller is running from macbook:
`source ~/halo/bin/env.sh && pipenv run python ~/halo/halo_unit 1 -c Tobis-MacBook-Pro.local`

### Access logs
```bash
tail -f halo/logs/cronlog_unit_1
```

## Run Tests
- `pipenv run python halo_controller/test/server.py`
