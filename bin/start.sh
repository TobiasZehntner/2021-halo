#!/bin/bash
# Make this file executable with `chmod 755 /home/pi/halo/bin/start.sh`
# Add it to cron: `sudo crontab -e`
# @reboot /bin/bash /home/pi/halo/bin/start.sh 1 >/home/pi/halo/logs/cronlog 2>&1

cron_enabled=true

if [ "$cron_enabled" = false ] ; then
    echo 'Cron disabled'
    exit 1
fi

export PYTHONPATH=/home/pi/halo:/usr/local/bin:$PATH
cd /home/pi/halo

if [[ $# -eq 0 ]] ; then
    echo 'No argument supplied (unit ID or "c" for controller)'
    exit 1
fi

if [ "$1" = "c" ]
then
  source /home/pi/halo/env.sh && /home/pi/.virtualenvs/halo-N-in2YQk/bin/python3 -m halo_controller halo-controller
else
  source /home/pi/halo/env.sh && /home/pi/.virtualenvs/halo-N-in2YQk/bin/python3 -m halo_unit $1
fi
